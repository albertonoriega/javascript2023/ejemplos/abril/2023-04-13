// Constante que apunta al lienzo ( canvas)
const lienzo = document.querySelector('#canvas');

// constante para dibujar en el lienzo
const contexto = lienzo.getContext("2d");

// dibujar rectángulos

// Rectangulo relleno 
contexto.fillRect(
    25, //x
    25, //y
    100, //ancho
    100 // alto
);
// Rectángulo de solo borde (x,y,ancho,alto)
contexto.strokeRect(130, 25, 100, 100);

//Cambiar el color con el que rellena

contexto.fillStyle = "rgb(200,0,0)";

//dibujamos un rectangulo que ahora tendrá un color de relleno rojo

contexto.fillRect(
    225, //x
    225, //y
    100, //ancho
    100 // alto
);

//cambiar el color del borde

contexto.strokeStyle = "rgb(0,150,50)";

contexto.strokeRect(325, 325, 60, 100);

/* Dibujar una forma con rectas */

contexto.beginPath(); // Comenzar la forma
contexto.moveTo( // nos posicionamos a la coordena 500,500 (punto origen)
    500, // coordenada X
    500 // coordenada Y
);
// Dibujamos una linea desde el punto en el que estaba (500,500) hasta la coordenada 600,600
contexto.lineTo(
    600,
    600
);
// Dibujamos una linea desde el punto en el que estaba (600,600) hasta la coordenada 450,550
contexto.lineTo(
    450,
    550
);
contexto.closePath(); // uniendo el punto de inicio con el punto origen

// Cerrar la forma y la rellena con el color de fondo
contexto.fill();
// Dibuja la forma sin relleno (solo bordes)
contexto.stroke();
// Si pones las dos, tienes relleno y borde