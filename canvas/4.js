// Constante que apunta al lienzo ( canvas)
const lienzo = document.querySelector('#canvas');

// constante para dibujar en el lienzo
const contexto = lienzo.getContext("2d");

//Cambiar el color con el que rellena a azul
contexto.fillStyle = "blue";

/* Vamos a dibujar un círculo*/

contexto.beginPath();
contexto.moveTo(400, 400);
contexto.arc(
    400, // coordenada X del centro del círculo
    400, // coordenada Y del centro de círculo
    100, // radio
    0, // ángulo de inicio
    2 * Math.PI // ángulo donde acaba
);
contexto.fill();

/* Un cuarto de circunferecia */
contexto.beginPath();
contexto.arc(100, 100, 50, 0, Math.PI / 2);
contexto.stroke();