// Constante que apunta al lienzo ( canvas)
const lienzo = document.querySelector('#canvas');

// constante para dibujar en el lienzo
const contexto = lienzo.getContext("2d");

//Cambiar el color con el que rellena a azul
contexto.fillStyle = "blue";

// Creamos un rectángulo con relleno
contexto.fillRect(
    100, //x
    100, //y
    300, //ancho
    300 // alto
);

//Cambiar el color con el que rellena a rojo
contexto.fillStyle = "red";

/* Dibujar un triángulo */

contexto.beginPath(); // Comenzar la forma
// Nos posicionamos en la coordenada donde va a empezar el triangulo (500, 400)
contexto.moveTo(
    500, // coordenada X
    400 // coordenada Y
);
// Dibujamos una linea desde el punto en el que estaba (500,400) hasta la coordenada 600,100
contexto.lineTo(
    600,
    100
);
// Dibujamos una linea desde el punto en el que estaba (600,600) hasta la coordenada 700,400
contexto.lineTo(
    700,
    400
);
contexto.closePath(); // uniendo el último punto con el punto origen

// Cerrar la forma y la rellena con el color de fondo
contexto.fill();

/*Estilos de los textos*/
//color
contexto.fillStyle = "black";
//tamaño
contexto.font = "20px serif";

//Coordenadas cuadrado
contexto.fillText("(100,100)", 70, 90);
contexto.fillText("(400,100)", 370, 90);
contexto.fillText("(400,400)", 350, 430);
contexto.fillText("(100,400)", 50, 430);

// Coordenadas del triángulo
contexto.fillText("(500,400)", 470, 430);
contexto.fillText("(700,400)", 670, 430);
contexto.fillText("(600,100)", 570, 90);