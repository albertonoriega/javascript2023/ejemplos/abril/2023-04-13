// constante que apunta al lienzo
const lienzo = document.querySelector('#canvas');

// constante para dibujar en el lienzo en dos dimensiones
const contexto = lienzo.getContext("2d");

// vamos a crear un objeto
const rectangulo = {
    x: 0,
    y: 0,
    ancho: 0,
    alto: 0,
    relleno: true,
    colorFondo: "rgb(0,0,0)",
    colorBorder: "rgb(0,0,0)",
    dibujar: function (contexto) {
        if (this.relleno) {
            contexto.fillStyle = this.colorFondo;
            contexto.fillRect(
                this.x,
                this.y,
                this.ancho,
                this.alto,
            );
        } else {
            contexto.strokeStyle = this.colorBorder;
            contexto.strokeRect(
                this.x,
                this.y,
                this.ancho,
                this.alto,
            );
        }

    },
};

// dibujar un rectágulo

rectangulo.x = 100;
rectangulo.y = 100;
rectangulo.ancho = 400;
rectangulo.alto = 200;

rectangulo.dibujar(contexto);


// dibujar otro rectangulo
rectangulo.x = 510;
rectangulo.ancho = 50;
rectangulo.colorFondo = "green";
rectangulo.dibujar(contexto);

//dibujar rectangulo sin relleno

rectangulo.x = 350;
rectangulo.y = 350;
rectangulo.ancho = 400;
rectangulo.alto = 200;
rectangulo.relleno = false;
rectangulo.colorBorder = "red";

rectangulo.dibujar(contexto);